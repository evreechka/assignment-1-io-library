exit:
    mov rax, 60
    syscall

string_length:
    .start:
        xor rax, rax
    .count:
        cmp byte [rax+rdi], 0
        je .end
        inc rax
        jmp .count
    .end:
        ret
print_string:
    .start:
    	push rdi
        call string_length
        pop rdi
    .print:
        mov     rsi, rdi
        mov     rdx, rax
        mov     rax, 1
        mov     rdi, 1
        syscall
    .end:
        ret

print_char:
    .start:
        push rsi
        push rax
        push rdx
        push rdi
    .print:
        mov rsi, rsp
        mov     rdx, 1
        mov     rax, 1
        mov     rdi, 1
        syscall
    .end:
        pop rdi
        pop rdx
        pop rax
        pop rsi
        ret

print_newline:
        mov rdi, 10
        call print_char
        ret

print_uint:
    .start:
        push rax
        push rdx
        push rsi
        mov rax, rdi
        mov rdi, 10
        mov rsi, rsp ; Save the address of rsp in rsi
        dec rsp
        mov [rsp], byte 0
    .int_to_string:
        xor rdx, rdx
        div rdi
        add rdx, '0'
        dec rsp
        mov byte [rsp], dl
        cmp rax, 0
        je .print_num
        jmp .int_to_string
    .print_num:
        mov rdi, rsp
        push rsi
        call print_string
        pop rsi
    .end:
        mov rsp, rsi
        pop rsi
        pop rdx
        pop rax
        ret

print_int:
    .start:
        cmp rdi, 0
        jns print_uint
    .print_minus:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
    .print_num:
        jmp print_uint

string_equals:
    .start:
        push rdx
        xor rdx, rdx
    .check_equals:
        cmp byte [rdi+rdx], 0
        je .equal
        xor rax, rax
        mov al, byte [rdx+rsi]
        cmp byte [rdi+rdx], al
        jne .not_equal
        inc rdx
        jmp .check_equals
    .equal:
        cmp byte [rsi+rdx], 0
        jne .not_equal
        mov rax, 0x1
        jmp .end
    .not_equal:
        mov rax, 0x0
    .end:
        pop rdx
        ret

read_char:
    .start:
        push rsi
        push rdi
        push rdx
        push 0x0
    .read:
        mov rsi, rsp
        mov rdi, 0
        mov rdx, 1
        mov rax, 0
        syscall
    .end:
        pop rax
        pop rdx
        pop rdi
        pop rsi
        ret

read_word:
    xor rax, rax
    push r8
    xor r8, r8
    .ignore_spaces:
    	push r8
        call read_char
        pop r8
        cmp rax, 0x20
        je .ignore_spaces
        cmp rax, 0x9
        je .ignore_spaces
        cmp rax, 0xA
        je .ignore_spaces
        test rax, rax
        jz .success
    .read:
        cmp r8, rsi
        ja .error
        cmp rax, 0x20
        je .success
        cmp rax, 0x9
        je .success
        cmp rax, 0xA
        je .success
        cmp rax, 0
        je .success
        mov [rdi+r8], rax
        inc r8
        push r8
        call read_char
        pop r8
        jmp .read
    .success:
        mov [rdi+r8+1], byte 0
        mov rax, rdi
        mov rdx, r8
        jmp .end
    .error:
        mov rax, 0
    .end:
        pop r8
        ret

parse_uint:
    .start:
        push rsi
        xor rax, rax
        xor rdx, rdx
    .parse:
        xor rsi, rsi
        mov sil, byte[rdi+rdx]
        cmp sil, '0'
        jb .end
        cmp sil, '9'
        ja .end
        sub sil, '0'
        imul rax, 10
        add rax, rsi
        inc rdx
        jmp .parse
    .end:
        pop rsi
        ret

parse_int:
    .check_sign:
        cmp byte[rdi], '-'
        jne parse_uint
    .invert:
        inc rdi
        call parse_uint
        cmp rdx, 0
        je .end
        neg rax
        inc rdx
    .end:
        ret

string_copy:
    .start:
        push r8
        push rcx
        call string_length
        inc rax
        cmp rdx, rax
        jl .error
        xor r8, r8
    .copy:
        xor rcx, rcx
        mov cl, byte[rdi+r8]
        mov byte[rsi+r8], cl
        inc r8
        cmp r8, rax
        jl .copy
        dec rax
        jmp .end
    .error:
        xor rax, rax
    .end:
        pop rcx
        pop r8
        ret

